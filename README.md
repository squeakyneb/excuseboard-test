excuseboard-test
===================

This repo is the test cases for the excuseboard. It's mostly garbage and can be ignored.

In general, the output of a test case will be two strings separated by a colon. The strings should match. `a1 a2:a1 a2` indicates a successful test (in that the problem they're created to exhibit has been fixed). `a1 a2:dog` indicates a failure. Due to the random nature of the program, tests may require multiple runs to actually have any value.

* aforward/abackward - tests that substrings aren't being replaced (thus ruining longer tokens), e.g. `$namesofcities` could be replaced by a `$names` to produce `Johnnyofcities` instead of `Rome`
* clobber1/clobber2 - tests that tokens with matching token names don't clobber each other